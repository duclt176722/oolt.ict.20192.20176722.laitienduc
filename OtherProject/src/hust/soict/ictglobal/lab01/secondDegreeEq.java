package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;;

public class secondDegreeEq {
	public static void main(String[] args) {
		double a,b,c;
		a= Double.parseDouble(JOptionPane.showInputDialog("Input a: "));
		b= Double.parseDouble(JOptionPane.showInputDialog("Input b: "));
		c= Double.parseDouble(JOptionPane.showInputDialog("Input c "));
		if(a==0) {
			if(b==0) {
			JOptionPane.showMessageDialog(null, "The eq has no solution");
		}
			else {
				JOptionPane.showMessageDialog(null, -c/b, "The eq has solution: ", JOptionPane.INFORMATION_MESSAGE );
			}
		}
		else {
			double del=b*b-4*a*c;
			if(del<0) {
				JOptionPane.showMessageDialog(null, "The eq has no solution");
			}
			else if(del==0) {
				JOptionPane.showMessageDialog(null, -b/(2*a),"The eq has unique solution", JOptionPane.INFORMATION_MESSAGE );
			}
			else {
				JOptionPane.showMessageDialog(null, (-b+ Math.sqrt(del))/(2*a) +" vs " + (-b- Math.sqrt(del))/(2*a), "The eq has solution: ", JOptionPane.INFORMATION_MESSAGE );
				//JOptionPane.showMessageDialog(null, (-b-Math.sqrt(del))/(2*a), "The eq has solution: ", JOptionPane.INFORMATION_MESSAGE );
			}
		}
		System.exit(1);
	}
}
