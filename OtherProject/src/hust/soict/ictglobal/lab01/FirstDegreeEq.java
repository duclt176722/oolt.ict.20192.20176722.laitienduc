package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;;
public class FirstDegreeEq {
	public static void main(String[] args) {
		double a,b;
		a= Double.parseDouble(JOptionPane.showInputDialog("Input: "));
		b= Double.parseDouble(JOptionPane.showInputDialog("Input: "));
		if(a==0) {
			JOptionPane.showMessageDialog(null, "The eq has no solution");
		}
		else {
			JOptionPane.showMessageDialog(null, -b/a, "The eq has solution: ", JOptionPane.INFORMATION_MESSAGE );
		}
		System.exit(1);
	}
}
