package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;;
public class system1stEq {
	public static void main(String[] args) {
		double a11,a12,a21,a22;
		double b1,b2;
		a11= Double.parseDouble(JOptionPane.showInputDialog("Input a11: "));
		a12= Double.parseDouble(JOptionPane.showInputDialog("Input a12: "));
		a21= Double.parseDouble(JOptionPane.showInputDialog("Input a21: "));
		a22= Double.parseDouble(JOptionPane.showInputDialog("Input a22: "));
		b1= Double.parseDouble(JOptionPane.showInputDialog("Input b1: "));
		b2= Double.parseDouble(JOptionPane.showInputDialog("Input b2: "));
		double D,D1,D2;
		D=a11*a22-a12*a21;
		D1=b1*a22-b2*a12;
		D2= a11*b2- a21*b2;
		if(D==0) {
			JOptionPane.showMessageDialog(null, "The eq has no solution");
		}
		else {
			JOptionPane.showMessageDialog(null, D1/D, "The first solution: ", JOptionPane.INFORMATION_MESSAGE );
			JOptionPane.showMessageDialog(null, D2/D, "The first solution: ", JOptionPane.INFORMATION_MESSAGE );
		}
		System.exit(1);
	}
}