package hust.soict.ictglobal.lab01;
import javax.swing.JOptionPane;
public class doubleNum {
	public static void main(String[] args) {
	double db1, db2;
	db1= Double.parseDouble(JOptionPane.showInputDialog(null, "Input the first double number", "Input", JOptionPane.INFORMATION_MESSAGE));
	db2= Double.parseDouble(JOptionPane.showInputDialog(null, "Input the second double number", "Input",JOptionPane.INFORMATION_MESSAGE));
	JOptionPane.showMessageDialog(null,  db1+db2,"Sum: ",  JOptionPane.INFORMATION_MESSAGE );
	JOptionPane.showMessageDialog(null,  db1-db2,"Subtract: ",  JOptionPane.INFORMATION_MESSAGE );
	JOptionPane.showMessageDialog(null,  db1*db2,"Product: ",  JOptionPane.INFORMATION_MESSAGE );
	if(db2==0) {
		JOptionPane.showMessageDialog(null,  " divisor must be diffirent from 0 "," ",  JOptionPane.ERROR_MESSAGE );
	}
	else {
		JOptionPane.showMessageDialog(null,  db1/db2,"Quotient: ",  JOptionPane.INFORMATION_MESSAGE );
	}
	}
}