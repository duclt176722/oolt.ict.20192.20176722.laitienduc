/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hust.soict.ictglobal.aims.test.media;

import hust.soict.ictglobal.aims.compare.sortCDbyNumofTracks;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.Track;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Rae Dookie
 */
public class TestTrackCompareTo {
    
    public static void main(String[] args) {
        ArrayList<CompactDisc> cdList = new ArrayList<CompactDisc>();
        CompactDisc cd1 = new CompactDisc("Westlife", "The Love Album", 50.85f);
        Track track11 = new Track(3, "My Love");
        Track track12 = new Track(4, "Nothing's Gonna Change My Love for You");
        Track track13 = new Track(5, "Seasion in the sun");
        Track track14 = new Track(6, "Beautiful in white");
        cd1.addTrack(track11);
        cd1.addTrack(track12);
        cd1.addTrack(track13);
        cd1.addTrack(track14);
        cdList.add(cd1);
        //------ cd2 --------
        CompactDisc cd2 = new CompactDisc("Justin Bieber", "Believe", 40.85f);
        Track track21 = new Track(2, "Baby");
        Track track22 = new Track(5, "Despacito");
        Track track23 = new Track(4, "Beauty and the Beat");
        cd2.addTrack(track21);
        cd2.addTrack(track22);
        cd2.addTrack(track23);
        cdList.add(cd2);
        //------ cd3 --------s
        CompactDisc cd3 = new CompactDisc("Imagine Dragon", "Origin", 70.41f);
        Track track31 = new Track(3, "Demons");
        Track track32 = new Track(5, "Believer");
        Track track33 = new Track(4, "Radioactive");
        Track track34 = new Track(4, "Bad Liar");
        Track track35 = new Track(5, "The best of the world");
        cd3.addTrack(track31);
        cd3.addTrack(track32);
        cd3.addTrack(track33);
        cd3.addTrack(track34);
        cd3.addTrack(track35);
        cdList.add(cd3);
        // ----------- cd4-----------
         CompactDisc cd4 = new CompactDisc("Westlife", "The Love Album", 50.85f);
        Track track41 = new Track(3, "My Love 1 ");
        Track track42 = new Track(4, "Nothing's Gonna Change My Love for You 2");
        Track track43 = new Track(8, "Seasion in the sun 3");
        Track track44 = new Track(7, "Beautiful in white 4");
        cd4.addTrack(track41);
        cd4.addTrack(track42);
        cd4.addTrack(track43);
        cd4.addTrack(track44);
        cdList.add(cd4);
        System.out.println(cdList.size());
        
        for (int i = 0; i < cdList.size(); i++) {
            System.out.print((i + 1) + ".");
            System.out.println(cdList.get(i).getArtist()+"-"+cdList.get(i).getTitle()+"-"+cdList.get(i).getNumofTrack());
            cdList.get(i).showAllTrack();
          
            System.out.println("Total length of cd:" + cdList.get(i).getTotalLength());
            System.out.println("");
        }
        
        System.out.println("*****************************");
        Collections.sort(cdList, new sortCDbyNumofTracks());
        System.out.println("======= After sort ======");
        System.out.println(cdList.size());
        
        for (int i = 0; i < cdList.size(); i++) {
            System.out.print((i + 1) + ".");
            System.out.println(cdList.get(i).getArtist()+"-"+cdList.get(i).getTitle()+"-"+cdList.get(i).getNumofTrack());
            cdList.get(i).showAllTrack();
          
            System.out.println("Total length of cd:" + cdList.get(i).getTotalLength());
            System.out.println("");
        }
    }
}
