package hust.soict.ictglobal.aims.interfaces;

public interface Playable {
    public void play();
}
