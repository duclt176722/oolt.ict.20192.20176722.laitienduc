package hust.soict.ictglobal.aims.compare;

import hust.soict.ictglobal.aims.media.CompactDisc;
import java.util.Comparator;

public class sortCDbyNumofTracks implements Comparator<CompactDisc> {

    @Override
    public int compare(CompactDisc cd1, CompactDisc cd2) {
        int d = cd1.getNumofTrack() - cd2.getNumofTrack();
        if (d > 0) {
            return 1;
        } else if (d < 0) {
            return -1;
        } else {
            int d1 = cd1.getLength() - cd2.getLength();
            if (d1 > 0) {
                return 1;
            } else if (d1 < 0) {
                return -1;
            } else {
                return 0;
            }
        }

    }
}
