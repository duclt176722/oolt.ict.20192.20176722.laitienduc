package hust.soict.ictglobal.aims.order;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.abstracts.Media;
import java.util.ArrayList;
import java.util.Date;

public class Order {
    
    public static final int MAX_NUMBER_ITEM = 10;  // max item in order
    public static final int MAX_LIMITTED_ORDER = 5; // max order 
    Date dateOrder = new Date();
    private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
    String dateOrdered;
    private int idOrder;
    private static int nbOrder = 0;

    private Order() {
        dateOrdered = dateOrder.toString();
        nbOrder++;
        if (nbOrder > MAX_LIMITTED_ORDER) {
            System.out.println("No more order");
        }
    }

    public static Order createOrder() {
        if (nbOrder > MAX_LIMITTED_ORDER) {
            System.out.println("No more order");
            return null;
        }
        return new Order();
    }

    public Date getDateOrder() {
        return dateOrder;
    }

    public void setDateOrder(Date dateOrder) {
        this.dateOrder = dateOrder;
    }

    public void showItem(Media med) {
        if (med instanceof DigitalVideoDisc) {
            DigitalVideoDisc tempDisc = (DigitalVideoDisc) med;
            tempDisc.showDVD();
        }
            if (med instanceof Book) {
            Book tempBook = (Book) med;
            tempBook.showBook();
        } 
            if(med instanceof CompactDisc) {
               
            ((CompactDisc) med).showAllTrack();
        }

    }

    // add media
    public void addMedia(Media med) {
        if (itemsOrdered.size() > MAX_NUMBER_ITEM) {
            System.out.println("The order is almost full");
        } else {
            for (int i = 0; i < itemsOrdered.size(); i++) {
                if (itemsOrdered.get(i).equals(med)) {
                    System.out.println("Item is in order, can't add");
                    return;
                }
            }
            itemsOrdered.add(med);
        }
    }

    // add media list
    public void addMedia(Media[] medList) {
        int count =0;
        for(int i=0; i< medList.length; i++){
            if(medList[i]!=null) count++;
        }
        if (itemsOrdered.size() + count > MAX_NUMBER_ITEM) {
            System.out.println("The order is almost full");
        } else {
            for (int i = 0; i < count; i++) {
                
                    itemsOrdered.add(medList[i]);
                
            }
        }
    }
    // add 2 media

    public void addMedia(Media med1, Media med2) {
        if (itemsOrdered.size() >= MAX_NUMBER_ITEM) {
            System.out.println("The order is almost full");
        } else if (itemsOrdered.size() + 1 >= MAX_NUMBER_ITEM) {
            System.out.println("Can add one more item");
            itemsOrdered.add(med1);
        } else {
            itemsOrdered.add(med1);
            itemsOrdered.add(med2);
        }
    }
   
    // show order
    public void showOrder() {
        System.out.println("***************** ORDER ********************");
        System.out.println("Date: " + dateOrdered);
        System.out.println("Order item");
        for (int i = 0; i < itemsOrdered.size(); i++) {
            System.out.print((i + 1) + ".");
            showItem(itemsOrdered.get(i));
        }
        totalCost();
    }

    //remove item
    public void removeMedia(int del) {
        for (int i = 0; i < itemsOrdered.size(); i++) {
            if ((i + 1) == del) {
                itemsOrdered.remove(i);
            }
        }
    }

    // total cost of oder
    public void totalCost() {
        float totalcost = 0;
        for (int i = 0; i < itemsOrdered.size(); i++) {
            totalcost += itemsOrdered.get(i).getCost();
        }
        System.out.print("Total cost is : ");
        System.out.println(totalcost);
    }

    // get lucky disc
//    public DigitalVideoDisc getALuckyItem() {
//        int max = qtyOrderd - 1;
//        int min = 0;
//        int range = max - min + 1;
//        int rand = (int) (Math.random() * range) + min;
//        itemsOrderd[rand].setCost(0f);
//        itemsOrderd[rand].setCategory(itemsOrderd[rand].getCategory() + "(--FREE--)");
//        return itemsOrderd[rand];
//    }
}
