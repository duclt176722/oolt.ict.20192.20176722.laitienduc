package hust.soict.ictglobal.aims.media;
import hust.soict.ictglobal.aims.abstracts.Media;
import  hust.soict.ictglobal.aims.interfaces.Playable;

public class DigitalVideoDisc extends Disc implements Playable{
    public DigitalVideoDisc() {
        super();
    }

    public DigitalVideoDisc(String title) {
        super(title);
    }

    public DigitalVideoDisc(String title, String category, float cost) {
        super(title, category, cost);
    }

    public DigitalVideoDisc(String title, String category) {
        super(title, category);
    }

   public DigitalVideoDisc(int length, String director, String title, String category, float cost) {
        super(length, director, title, category, cost);
    }
   public DigitalVideoDisc(int length, String director, String title, String category, float cost, String id) {
        super(length, director, title, category, cost,id);
    }
   // =================== search dvd ================
    public boolean search(String title) {
        title = title.toLowerCase();
        title = title.trim().replaceAll("\\s+", " ");
        String temp = this.getTitle().toLowerCase();
        if (temp.equals(title)) {
            return true;
        }
        String[] arrTitle = title.split(" ");
        int count = 0;
        for (String item : arrTitle) {
            if (temp.contains(item)) {
                count++;
            }
        }
        if (count == arrTitle.length) {
            return true;
        }
        return false;
    }
    // ==========================
    public void showDVD(){
        System.out.println("DVD-"+this.getIdMedia()+"-"+ this.getTitle() + "-" + this.getLength() + "mins-" + this.getDirector()+"-" + this.getCost() + "-" + this.getCategory());
    }
    
     @Override
    public void play() {
        System.out.println("Playing DVD: " + this.getTitle());
        System.out.println("DVD length: " + this.getLength());
    }
    @Override
    public int compareTo(Media o) {
        DigitalVideoDisc temp= (DigitalVideoDisc) o;
//        // ----- sort dvd by title ----------
//        //return this.getTitle().compareTo(temp.getTitle());
//         // ----- sort dvd by cost ------------
            float d= this.getCost() - temp.getCost();
            if(d>0) return 1;
            else if(d<0) return -1;
            else return 0;
    
    }
}
