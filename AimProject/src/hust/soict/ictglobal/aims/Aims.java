package hust.soict.ictglobal.aims;

import hust.soict.ictglobal.aims.media.Book;
import hust.soict.ictglobal.aims.media.CompactDisc;
import hust.soict.ictglobal.aims.media.DigitalVideoDisc;
import hust.soict.ictglobal.aims.abstracts.Media;
import hust.soict.ictglobal.aims.media.Track;
import hust.soict.ictglobal.aims.order.Order;
import hust.soict.ictglobal.aims.thread.MenoryDaemon;
import java.util.ArrayList;
import java.util.Scanner;

public class Aims {

    static ArrayList<DigitalVideoDisc> dvdList = new ArrayList<DigitalVideoDisc>();
    static ArrayList<Book> bookList = new ArrayList<Book>();
    static ArrayList<CompactDisc> cdList = new ArrayList<CompactDisc>();
    static Scanner scanner = new Scanner(System.in);
    static ArrayList<Order> anOrder = new ArrayList<Order>();

    public static void showMenuManager() {
        System.out.println("===========================================");
        System.out.println("Store Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Add item to the store");
        System.out.println("2. Delete item by id");
        System.out.println("3. Display the items ");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3");
    }

    public static void showRole() {

        System.out.println("--------------------------------");
        System.out.println("1. Customer");
        System.out.println("2. Store Manager");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2");
    }

    public static void showMenuCustomer() {
        System.out.println("==============================================");
        System.out.println("Order Management Application: ");
        System.out.println("--------------------------------");
        System.out.println("1. Create new order");
        System.out.println("2. Add item to the order");
        System.out.println("3. Delete item by id");
        System.out.println("4. Display the items list of order");
        System.out.println("0. Exit");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 0-1-2-3-4");
    }

    public static void showMenuItem() {
        System.out.println(" ---------- MENU ITEM ----------");
        System.out.println("1. Compact Disc");
        System.out.println("2. Digital Video Disc");
        System.out.println("3. Book");
        System.out.println("--------------------------------");
        System.out.println("Please choose a number: 1-2-3");
    }

    public static void addMediaStore() {
        int select;
        do {
            showMenuItem();
            System.out.println("Enter item to add");
            select = scanner.nextInt();
            if (select > 4 || select < 1) {
                System.out.println("Enter selection item again");
            }

        } while (select > 4 || select < 1);
        scanner.nextLine();
        switch (select) {
            case 1: // ----- CDs ------
                System.out.println("Input name of artist:");
                String artist = scanner.nextLine();
                System.out.println("Input cost of CDs:");
                float costCD = scanner.nextFloat();
                scanner.nextLine();
                System.out.println("Input title of CD:");
                String tilteCD = scanner.nextLine();
                String idCD ;
                if(cdList.size()<10){
                    idCD="CD0"+(cdList.size()+1);
                }
                else{
                    idCD = "CD"+(cdList.size()+1);
                }
                CompactDisc compact = new CompactDisc(artist, tilteCD, costCD,idCD);
                System.out.println("Input the track of the compact disc: ");
                String text;
                do {
                    System.out.println("Input title track:");
                    String titleTrack = scanner.nextLine();
                    System.out.println("Input length of track:");
                    int lengthTrack = scanner.nextInt();
                    scanner.nextLine();
                    Track track = new Track(lengthTrack, titleTrack);
                    compact.addTrack(track);
                    System.out.println("Do you want to add other track? (Y/N)");
                    text = scanner.nextLine().toLowerCase();
                } while (text.equals("y"));

                cdList.add(compact);
                break;
            case 2: //------ DVD --------
                System.out.println("Input title of digital video disc:");
                String titleDVD = scanner.nextLine();
                System.out.println("Input catagory of digital video disc:");
                String categoryDVD = scanner.nextLine();
                System.out.println("Input cost of digital video disc:");
                float costDVD = scanner.nextFloat();
                scanner.nextLine();
                System.out.println("Input length of digital video disc:");
                int lengthDVD = scanner.nextInt();
                scanner.nextLine();
                System.out.println("Input director of digital video disc:");
                String directorDVD = scanner.nextLine();
                String idDVD ;
                if(dvdList.size()<10){
                    idDVD="DVD0"+(dvdList.size()+1);
                }
                else{
                    idDVD = "DVD"+(dvdList.size()+1);
                }
                DigitalVideoDisc disc = new DigitalVideoDisc(lengthDVD, directorDVD, titleDVD, categoryDVD, costDVD,idDVD);
                dvdList.add(disc);
                break;
            case 3: // ----- book -------
                System.out.println("Input title of book:");
                String titleBook = scanner.nextLine();
                System.out.println("Input category of book:");
                String categoryBook = scanner.nextLine();
                System.out.println("Input cost of book:");
                float costBook = scanner.nextFloat();
                scanner.nextLine();
                String idBook ;
                if(bookList.size()<10){
                    idBook="B0"+(bookList.size()+1);
                }
                else{
                    idBook = "B"+(bookList.size()+1);
                }
                Book book = new Book(titleBook, categoryBook, costBook,idBook);
                System.out.println("Input author of book: ");
                String exit;
                do {
                    scanner.nextLine();
                    String tempAuthor;
                    System.out.println("Author: ");
                    tempAuthor = scanner.nextLine();
                    book.addAuthor(tempAuthor);
                    System.out.println("Add another author?? (Y/N)");
                    exit = scanner.next().toLowerCase();

                } while (exit.equals("y"));
                bookList.add(book);
                break;
        }
    }

    public static void removeMediaStore() {
        int select;
        do {
            showMenuItem();
            System.out.println("Enter item to add");
            select = scanner.nextInt();
            if (select > 4 || select < 1) {
                System.out.println("Enter selection item again");
            }

        } while (select > 4 || select < 1);
        scanner.nextLine();
        switch (select) {
            case 1:
                showCD(cdList);
                System.out.println("Input id to remove");
                int rm = scanner.nextInt();
                scanner.nextLine();
                cdList.remove(rm - 1);
                break;
            case 2:
                showDVD(dvdList);
                System.out.println("Input id to remove");
                rm = scanner.nextInt();
                scanner.nextLine();
                dvdList.remove(rm - 1);
                break;
            case 3:
                showBook(bookList);
                System.out.println("Input id to remove");
                rm = scanner.nextInt();
                scanner.nextLine();
                bookList.remove(rm - 1);
                break;
            default:
                break;
        }
    }

    public static Media addItems() {
        int select;
        int choise;
        do {
            showMenuItem();
            System.out.println("Enter item to add");
            select = scanner.nextInt();
            if (select > 4 || select < 1) {
                System.out.println("Enter selection item again");
            }

        } while (select > 4 || select < 1);
        scanner.nextLine();
        switch (select) {
            case 1: // -------- CD -----------
                showCD(cdList);
                do {
                    System.out.println("Select item: ");
                    choise = scanner.nextInt();
                    if (choise < 0 || choise > cdList.size()) {
                        System.out.println("Select again!");
                    }
                } while (choise < 0 || choise > cdList.size());
                scanner.nextLine();
                System.out.println("Do you want to play CD? ( Y/N )");
                String choose = scanner.nextLine().toLowerCase();
                if (choose.equals("y")) {
                    cdList.get(choise - 1).play();
                }
                return cdList.get(choise - 1);
               

            case 2: // ------------ DVD -------------
                int choiseUser;
                System.out.println("==========================");
                System.out.println("1. Search by title");
                System.out.println("2. Select");
                do {
                    System.out.println("Select item: ");
                    choiseUser = scanner.nextInt();
                    scanner.nextLine();
                    if (choiseUser < 0 || choiseUser > 2) {
                        System.out.println("Select again!");
                    }
                } while (choiseUser < 0 || choiseUser > 2);
                switch (choiseUser) {
                    case 1:
                        System.out.println("Input the token or title");
                        String title;
                        ArrayList<Integer> arr= new ArrayList<>();
                        title= scanner.nextLine();
                        for (int i = 0; i < dvdList.size(); i++) {
                            if (dvdList.get(i).search(title)) {
                                System.out.print((i + 1) + ".");
                                dvdList.get(i).showDVD();
                                arr.add(i+1);
                            }
                        }
                        int d;
                        do {
                            d=0;
                            System.out.println("Select item: ");
                            choise = scanner.nextInt();
                            for(int i=0; i<arr.size(); i++){
                                if(arr.get(i) == choise) d++;
                            }
                            if(d==0) System.out.println("Select again!");
                        } while (d==0);
                        
                        scanner.nextLine();
                        System.out.println("Do you want to play CD? ( Y/N )");
                        choose = scanner.nextLine().toLowerCase();
                        if (choose.equals("y")) {
                            dvdList.get(choise - 1).play();
                        }
                        return dvdList.get(choise - 1);
                        
                        
                    case 2:
                        showDVD(dvdList);
                        do {
                            System.out.println("Select item: ");
                            choise = scanner.nextInt();
                            if (choise < 0 || choise > dvdList.size()) {
                                System.out.println("Select again!");
                            }
                        } while (choise < 0 || choise > dvdList.size());
                        scanner.nextLine();
                        System.out.println("Do you want to play CD? ( Y/N )");
                        choose = scanner.nextLine().toLowerCase();
                        if (choose.equals("y")) {
                            dvdList.get(choise - 1).play();
                        }
                        return dvdList.get(choise - 1);
                }

            case 3: // ----------- Book ---------------
                showBook(bookList);
                do {
                    System.out.println("Select item: ");
                    choise = scanner.nextInt();
                    if (choise < 0 || choise > bookList.size()) {
                        System.out.println("Select again!");
                    }
                } while (choise < 0 || choise > bookList.size());
                scanner.nextLine();
                return bookList.get(choise - 1);
        }
        return null;

    }

    public static void showOrderList(ArrayList<Order> orders) {
        for (int i = 0; i < orders.size(); i++) {
            System.out.print((i + 1) + ".");
            orders.get(i).showOrder();
        }
    }

    public static void showDVD(ArrayList<DigitalVideoDisc> dvdList) {
        System.out.println(dvdList.size());
        for (int i = 0; i < dvdList.size(); i++) {
            System.out.print((i + 1) + ".");
            dvdList.get(i).showDVD();
        }
        System.out.println("*****************************");

    }

    public static void showBook(ArrayList<Book> bookList) {
        System.out.println(bookList.size());
        for (int i = 0; i < bookList.size(); i++) {
            System.out.print((i + 1) + ".");
            bookList.get(i).showBook();
        }
        System.out.println("*****************************");
    }

    public static void showCD(ArrayList<CompactDisc> cdList) {
        System.out.println(cdList.size());
        for (int i = 0; i < cdList.size(); i++) {
            System.out.print((i + 1) + "."+cdList.get(i).getIdMedia()+"-");
            cdList.get(i).showAllTrack();
        }
        System.out.println("*****************************");
    }

    public static void initial() {
        // ====== dvd list in store =======

        DigitalVideoDisc dvd1 = new DigitalVideoDisc(87, "Roger Alller", "The lion king", "Animation", 19.95f,"DVD01");
        DigitalVideoDisc dvd2 = new DigitalVideoDisc(124, "George Lucas", "Star Wars", "Scient Fiction", 24.55f, "DVD02");
        DigitalVideoDisc dvd3 = new DigitalVideoDisc(101, "John Musker", "Aladdin", "Animation", 18.99f, "DVD03");
        DigitalVideoDisc dvd4 = new DigitalVideoDisc(110, "Bong Joon-ho", "Parasite", " Comedy, Drama, Thriller ", 50.99f,"DVD04");
        DigitalVideoDisc dvd5 = new DigitalVideoDisc(120, "Todd Phillips", "Joker", " Comedy, Drama, Thriller ", 80.99f,"DVD05");
        DigitalVideoDisc dvd6 = new DigitalVideoDisc(120, "John Lucas", "World War Z", "Animation ", 79.9f,"DVD06");
        dvdList.add(dvd1);
        dvdList.add(dvd2);
        dvdList.add(dvd3);
        dvdList.add(dvd4);
        dvdList.add(dvd5);
        dvdList.add(dvd6);
//       for(int i=0; i< dvdList.size(); i++){
//           dvdList.get(i).showDVD();
//       }
        //========= books list in store ==========

        Book book1 = new Book("Sherlock Holmes", "Detective", 22.3f,"B01");
        book1.addAuthor("Conan Doyle");
        Book book2 = new Book("Harry Potter and the Deathly Hallows ", "Adventure", 12.3f,"B02");
        book2.addAuthor("J.K. Rowling");
        Book book3 = new Book("One Piece ", "Comic", 10.5f,"B03");
        book3.addAuthor("Eiichiro Oda");
        Book book4 = new Book("Let It Snow", "Comic", 20.5f,"B04");
        book4.addAuthor("John Green");
        book4.addAuthor("Maureen Johnson");
        book4.addAuthor("Lauren Myracle");
        bookList.add(book1);
        bookList.add(book2);
        bookList.add(book3);
        bookList.add(book4);

        //========= CDs list in store==========
        //------ cd1 --------
        CompactDisc cd1 = new CompactDisc("Westlife", "The Love Album", 50.85f,"CD01");
        Track track11 = new Track(3, "My Love");
        Track track12 = new Track(4, "Nothing's Gonna Change My Love for You");
        Track track13 = new Track(5, "Seasion in the sun");
        Track track14 = new Track(6, "Beautiful in white");
        cd1.addTrack(track11);
        cd1.addTrack(track12);
        cd1.addTrack(track13);
        cd1.addTrack(track14);
        cdList.add(cd1);
        //------ cd2 --------
        CompactDisc cd2 = new CompactDisc("Justin Bieber", "Believe", 40.85f,"CD02");
        Track track21 = new Track(2, "Baby");
        Track track22 = new Track(5, "Despacito");
        Track track23 = new Track(4, "Beauty and the Beat");
        cd2.addTrack(track21);
        cd2.addTrack(track22);
        cd2.addTrack(track23);
        cdList.add(cd2);
        //------ cd3 --------
        CompactDisc cd3 = new CompactDisc("Imagine Dragon", "Origin", 70.41f,"CD03");
        Track track31 = new Track(3, "Demons");
        Track track32 = new Track(5, "Believer");
        Track track33 = new Track(4, "Radioactive");
        Track track34 = new Track(4, "Bad Liar");
        Track track35 = new Track(5, "The best of the world");
        cd3.addTrack(track31);
        cd3.addTrack(track32);
        cd3.addTrack(track33);
        cd3.addTrack(track34);
        cd3.addTrack(track35);
        cdList.add(cd3);

        //========================================
//        showBook(bookList);
//        showCD(cdList);
//        showDVD(dvdList);
    }

    public static void showManager() {
        int choise;
        do {
            System.out.println(" ---------- MENU ITEM ----------");
            System.out.println("1. Compact Disc");
            System.out.println("2. Digital Video Disc");
            System.out.println("3. Book");
            System.out.println("4. Show all");
            System.out.println("0. Exit");
            System.out.println("--------------------------------");
            do {
                System.out.println("Enter selection");
                choise = scanner.nextInt();
                scanner.nextLine();
                if (choise > 4 || choise < 0) {
                    System.out.println("Enter selection again");
                }
            } while (choise > 4 || choise < 0);
            switch (choise) {
                case 1:
                    showCD(cdList);
                    break;
                case 2:
                    showDVD(dvdList);
                    break;
                case 3:
                    showBook(bookList);
                    break;
                case 4:
                    showCD(cdList);
                    showDVD(dvdList);
                    showBook(bookList);
                    break;
                default:
                    break;
            }
        } while (choise != 0);

    }

    public static void main(String[] args) {
        int choise;
        int choiseCustomer;
        int choiseManager;
        int count = -1;
        String password = "oop20192";
        String pwManager;
        MenoryDaemon newMenoryDaemon = new MenoryDaemon();
        initial();
        do {
            showRole();
            System.out.println("Enter selection");
            choise = scanner.nextInt();
            scanner.nextLine();
            if (choise > 2 || choise < 0) {
                System.out.println("Enter selection again");
            }

            switch (choise) {
                // =========== Customer ===============
                case 1:
                    do {
                        showMenuCustomer();
                        System.out.println("Enter selection");
                        choiseCustomer = scanner.nextInt();
                        scanner.nextLine();
                        if (choiseCustomer > 4 || choiseCustomer < 0) {
                            System.out.println("Enter selection again");
                        }
                        switch (choiseCustomer) {
                            case 1:
                                anOrder.add(Order.createOrder());
                                System.out.println("~~~~ Order is created ~~~~");
                                count++;
                                break;
                            case 2:
                                
                                    anOrder.get(count).addMedia(addItems());
                                break;
                            case 3:
                                System.out.println("Input id to delete:");
                                int del = scanner.nextInt();
                                scanner.nextLine();
                                anOrder.get(count).removeMedia(del);
                                break;
                            case 4:
                                showOrderList(anOrder);
                                break;
                            default:
                                break;

                        }
                    } while (choiseCustomer != 0 || choiseCustomer > 4);
                    break;

                // =========== Store Manager ===============
                case 2:

                    System.out.println("If you're store manager, please enter the password:");
                    pwManager = scanner.nextLine();
                    if (!pwManager.equals(password)) {
                        System.err.println("Wrong password");
                        break;
                    } else {
                        do {
                            showMenuManager();
                            System.out.println("Enter selection");
                            choiseManager = scanner.nextInt();
                            scanner.nextLine();
                            if (choiseManager > 3 || choiseManager < 0) {
                                System.out.println("Enter selection again");
                            }
                            switch (choiseManager) {
                                case 1:
                                    addMediaStore();
                                    break;
                                case 2:
                                    removeMediaStore();
                                    break;
                                case 3:
                                    showManager();
                                    break;
                                default:
                                    break;

                            }
                        } while (choiseManager != 0 || choiseManager > 3);
                        break;
                    }
                default:
                    break;
            }
        } while (choise != 0 || choise > 2);

        //newMenoryDaemon.run();
        System.exit(0);
    }
}
